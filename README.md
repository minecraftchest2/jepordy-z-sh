# Jepordy.sh
A simple, lightweight jepordy server in bash

## Basic Usage

Update questions.txt with your questions and category names. Run
main.sh `./main.sh` or `bash main.sh`. The web server will listen
on port 2001.
